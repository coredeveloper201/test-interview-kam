<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Session;
use Hash;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['login', 'processLogin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ( $request->user()->authorizeRoles(['system_admin']) ) 
        {
            $view = view('home');
        }
        else if ( $request->user()->authorizeRoles(['admin']) ) 
        {
            $view = view('home_admin');
        }
        else if ( $request->user()->authorizeRoles(['manager']) )
        {
            $view = view('home_manager');
        }
        else if ( $request->user()->authorizeRoles(['customer']) )
        {
            $view = view('home_customer');
        }

        return $view;
    }

    /**
     * Create admin function
     *
     */
    public function createAdmin(Request $request)
    {
        $this->adminValidation($request);

        $name = $request->get('name');
    	$email = $request->get('email');
    	$password = $request->get('password');
        
        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        $user->roles()
            ->attach(Role::where('name', 'admin')->first());

    	Session::flash('msg', 'Admin created successfully');
    	return back();
    }

    /**
     * Validation for admin creation
     */
    public function adminValidation($request)
    {
        $rules = [
    		'name' 		=> 'required',
    		'email' 	=> 'required|email'
    	];

    	$custom_message = [
    		'name.required' 	=> 'Name cannot be empty',
    		'email.required' 	=> 'E-mail cannot be empty'
    	];

    	return $this->validate($request, $rules, $custom_message);
    }

    /**
     * Login form
     */
    public function login()
    {
        return view('login');
    }

    /**
     * Process login submission
     */
    public function processLogin(Request $request)
    {
        $email    = $request->email;
        $password = $request->password;

        $user = User::where('email', '=', $email)->first();

        // if ( isset($user->password) && Hash::check($password, $user->password) )
        if ( Auth::attempt(['email' => $email, 'password' => $password]) )
        {
            // dd($request->user()->authorizeRoles(['system_admin']));
            // Get role id by user id
            $role_id = DB::table('role_user')
                            ->where('user_id', $user->id)
                            ->get();

            if ( $role_id[0]->role_id == 1 ) // system_admin 
            {
                $view = view('home');
            }
            else if ( $role_id[0]->role_id == 2 ) // admin
            {
                $view = view('home_admin');
            }
            else if ( $role_id[0]->role_id == 3 ) // manager
            {
                $view = view('home_manager');
            }
            else if ( $role_id[0]->role_id == 4 ) // customer
            {
                $view = view('home_customer');
            }

            return $view;
        }
        else 
        {
            Session::flash('error-msg', 'Invalid Login!');
            return back();
        }
    }

}
