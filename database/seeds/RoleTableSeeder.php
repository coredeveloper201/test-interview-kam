<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = 'system_admin';
        $role_employee->description = 'A System admin';
        $role_employee->save();

        $role_manager = new Role();
        $role_manager->name = 'admin';
        $role_manager->description = 'An Admin';
        $role_manager->save();
        
        $role_manager = new Role();
        $role_manager->name = 'manager';
        $role_manager->description = 'A Manager';
        $role_manager->save();
        
        $role_manager = new Role();
        $role_manager->name = 'customer';
        $role_manager->description = 'A Customer';
        $role_manager->save();
    }
}
