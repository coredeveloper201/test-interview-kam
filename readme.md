<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## About this project

Laravel 4 types user authentication system with user creation.

## System information

With the below route developer will get custom login form, all users will go through this login 
process and login will check current user is system admin, admin, manager or customer.
After successfull login those 4 types of users will get their own views home, home_admin, home_manager and home_customer respectively.
/mainlogin

For registration, laravel's auth:make's registration is working here but with only one differnece in create method, role is getting attached here as system_admin as default role.

## How to seeding data

Run the below command to seed fake data:
php artisan migrate:fresh --seed

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
